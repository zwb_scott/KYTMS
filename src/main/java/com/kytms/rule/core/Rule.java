package com.kytms.rule.core;

import com.kytms.rule.core.exception.RuleException;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 *
 * 规则
 *
 * @author 臧英明
 * @create 2018-05-07
 */
public interface Rule<T> {
    /**
     * 编译，将一段字符串进行编译成规则模型
     * @param str
     * @return
     */
    T toCompile(String str) throws RuleException;

    /**
     * 执行规则
     * @param rullModel
     * @return
     */
    Object executeRule(RuleModel rullModel) throws RuleException;
}
