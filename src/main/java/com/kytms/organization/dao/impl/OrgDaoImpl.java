package com.kytms.organization.dao.impl;

import com.kytms.core.dao.impl.BaseDaoImpl;
import com.kytms.core.entity.Organization;
import com.kytms.organization.action.OrgAction;
import com.kytms.organization.dao.OrgDao;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

/**
 * 辽宁捷畅物流有限公司 -信息技术中心
 * @author 臧英明
 * @create 2017-11-23
 */
@Repository(value = "OrgDao")
public class OrgDaoImpl extends BaseDaoImpl<Organization> implements OrgDao<Organization> {
    private final Logger log = Logger.getLogger(OrgDaoImpl.class);//输出Log日志
}
